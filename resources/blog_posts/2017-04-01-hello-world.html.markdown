---
title: Hello, World!
posted_at: "2017-04-01T20:00:00.00+02:00[Europe/Berlin]"
location: Berlin, Germany
main_image: /images/earth-at-night.jpg
description: The very first ever blog post for Write Honey. This details our mission and motivations.

---

I'm not sure if I'm doing this because it's a good practice for any blogger or because I'm just so darn enthusiastic, but I'm proud to formally announce the existence of the Write Honey writing blog, which I've named _Write Free_.

## Write Free or Die

The name of the blog comes from the main benefits that I currently associate with free writing, which is liberation from stirring thoughts and concerns. There have been many times in my own experience that I've felt overwhelmed with what I had to do, and sometihng as simple as journaling helped me feel more prepared to and confident in taking on my challenges.


## Setting Expectations

This blog will exist to serve as a resource for understanding the benefits of free writing better, and to then provide you the resources for free writing without a hitch! The obvious first step would be to check out the product that this blog supports at https://writehoney.com/

I look forward to serving you all with only the highest quality content, because I want you to gain the same amazing benefits of free writing that I've gained since I started taking up this habit.
