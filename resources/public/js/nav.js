$(document).ready(function() {
  $("a#nav-toggle").click(function(e) {
    e.preventDefault();
    $(this).toggleClass('active');
    $('.mini-menu').toggleClass('active');
  });
});
