(ns compound-eye.app
  (:require
   [compound-eye.blog      :as blog]
   [compound-eye.layout    :as layout]
   [compound-eye.sitemap   :as sitemap]
   [compound-eye.view.blog :as view.blog]
   [compound-eye.view.page :as view.page]
   ))

(defn get-blog-pages []
  (->> (blog/all-blog-posts)
       (map (juxt :path (comp layout/blog-layout view.blog/blog-post) (constantly 0.5)))
       (map (partial zipmap [:path :output :priority]))
       (concat [{:path     "/blog/"
                 :output   ((comp layout/blog-layout view.blog/blog-root))
                 :priority 0.9}])))

(defn get-standard-pages []
  (->> [view.page/index
        view.page/product
        view.page/free-writing-page
        view.page/alternative-to-750-words-page
        view.page/free-writing-page]
       (map (juxt :path layout/page-layout :priority))
       (map (partial zipmap [:path :output :priority]))))

(defn get-static-text-files []
  [{:path "/robots.txt"
    :output "User-agent: *\nDisallow:\nSitemap: https://writehoney.com/sitemap.xml"}
   {:path "/1-beef-faced-facade.txt" ;; IndexNow key
    :output "1-beef-faced-facade"}])

(defn get-pages []
  (->> (concat
        (get-blog-pages)
        (get-standard-pages)
        (get-static-text-files))
       (sitemap/wrap-sitemap)
       (map (juxt :path :output))
       (into {})))
