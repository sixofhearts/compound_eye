(ns compound-eye.blog
  (:require
   [clojure.string    :as str]
   [clojure.java.io   :as io]
   [compound-eye.time :as time]
   [frontmatter.core  :as frontmatter]
   ))

(def ^:private blog-post-filename-slug-pattern
  #"[0-9]{4}-[0-9]{2}-[0-9]{2}-(.+).html.markdown")

(def ^:private author-data
  (read-string (slurp "resources/paul-data.edn")))

(defn- blog-post-file->slug [blog-post-file]
  (as-> blog-post-file $
    (.getName $)
    (re-matches blog-post-filename-slug-pattern $)
    (second $)
    (str "/blog/" $ "/")))

(defn- blog-post-file->blog-post [blog-post-file]
  (let [{:keys [body frontmatter]} (frontmatter/parse blog-post-file)]
    (-> frontmatter
        (update :posted_at time/timestring->zdt)
        (assoc :path (blog-post-file->slug blog-post-file)
               :body body
               :author author-data))))

(defn- all-blog-post-files []
  (->> (file-seq (io/file "resources/blog_posts"))
       (filter #(.endsWith (.getName %) ".html.markdown"))))

(defn- blog-post= [& blog-posts]
  (apply = (map :path blog-posts)))

(defn all-blog-posts []
  (->> (all-blog-post-files)
       (map blog-post-file->blog-post)
       (sort-by :posted_at)
       reverse))

(defn next-blog-post [blog-post]
  (->> (all-blog-posts)
       (take-while (comp not (partial blog-post= blog-post)))
       last))

(defn prev-blog-post [blog-post]
  (->> (all-blog-posts)
       reverse
       (take-while (comp not (partial blog-post= blog-post)))
       last))
