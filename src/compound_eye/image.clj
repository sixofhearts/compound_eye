(ns compound-eye.image
  (:require
   [clojure.java.io :as io]
   [image-resizer.format :as format]
   [image-resizer.core :as resizer]
   ))

(defn resize-image! [src-filename dest-filename width height]
  (format/as-file
   (resizer/resize (io/file src-filename) width height)
   dest-filename
   :verbatim))

;; Favicon Specific

(def ^:private favicon-source
  "resources/source_images/bee.png")

(def png-favicon-dimensions
  [16 32 48])

(def apple-touch-icon-dimensions
  [57 114 72 144 60 120 76 152])

(def public-directory
  "/icons/")

(defn- generate-filename [prefix dimension]
  (str prefix "__" dimension "x" dimension ".png"))

(defn png-favicons []
  (for [dimension png-favicon-dimensions]
    {:dimension dimension
     :path (generate-filename (str public-directory "favicon") dimension)}))

(defn apple-touch-icons []
  (for [dimension apple-touch-icon-dimensions]
    {:dimension dimension
     :path (generate-filename (str public-directory "apple-touch-icon") dimension)}))

(defn build-all-icons! []
  (doseq [{:keys [dimension path]} (concat (png-favicons) (apple-touch-icons))]
    (let [dest-filename (str "resources/public" path)]
      (resize-image! favicon-source dest-filename dimension dimension))))


(comment
  (build-all-icons!)
  )
