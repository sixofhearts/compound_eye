(ns compound-eye.time
  (:import
   [java.time ZonedDateTime]
   [java.time.format DateTimeFormatter]
   ))

(def readable-string-date-format
  (DateTimeFormatter/ofPattern "EEEE, MMMM d, yyyy"))

(defn zdt->readable-string-date [zdt]
  (.format readable-string-date-format zdt))

(defn timestring->zdt [timestring]
  (ZonedDateTime/parse timestring))
