(ns compound-eye.core
  (:require
   [ring.middleware.content-type :refer [wrap-content-type]]
   [stasis.core :as stasis]
   [optimus.prime :as optimus]
   [optimus.assets :as assets]
   [optimus.optimizations :as optimizations]
   [optimus.strategies :refer [serve-live-assets]]
   [optimus.export]
   [compound-eye.app :as app]
   ))

(def target-dir
  "Where to tuck these files into"
  "target/build")

(defn get-assets []
  (assets/load-assets "public" ["/css/main.css"
                                "/favicon.ico"
                                #"/js/.*.js"
                                #"/fonts/.*.ttf"
                                #"/images/.*"
                                #"/icons/.*"]))

(def app
  "Server"
  (-> (stasis/serve-pages app/get-pages)
      (optimus/wrap get-assets optimizations/none serve-live-assets)
      wrap-content-type))


(defn export
  "Exports files"
  []
  (let [assets (optimizations/all (get-assets) {})
        pages (app/get-pages)]
    ;;(stasis/empty-directory! target-dir)
    (optimus.export/save-assets assets target-dir)
    (stasis/export-pages pages target-dir {:optimus-assets assets})))
