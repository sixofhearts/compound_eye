(defproject compound_eye "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.10.1"]
                 [frontmatter "0.0.1"]
                 [hiccup "2.0.0-alpha2"]
                 [image-resizer "0.1.10"]
                 [markdown-clj "1.11.2"]
                 [optimus "2022-02-13"]
                 [org.clojure/data.xml "0.0.8"]
                 [ring "1.9.5"]
                 [stasis "2.5.1"]]
  :plugins [[lein-ring "0.12.6"]]
  :ring {:handler compound-eye.core/app}
  :aliases {"build-site" ["run" "-m" "compound-eye.core/export"]})
